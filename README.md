## Meraki CI pipeline with ansible

### Notes
- docker image created with python 3.8 base, ansible modules and meraki collections added to image

- image stored in gitlab container registry

- local gitlab runner provisioned on lab linux VM

- secret stored as masked variable for CI/CD, executor stores as environment variabled used by ansible lookup for meraki API KEY
