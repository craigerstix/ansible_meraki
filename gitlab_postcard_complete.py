from webexteamssdk import WebexTeamsAPI
from webexteamssdk.models.cards.card import AdaptiveCard
from webexteamssdk.models.cards.components import TextBlock, Image, Fact
from webexteamssdk.models.cards.container import FactSet
from webexteamssdk.utils import make_attachment
from webexteamssdk.models.cards.options import ImageSize
import datetime 

room_id = "Y2lzY29zcGFyazovL3VzL1JPT00vNGM5MjY5NzAtZGM1NC0xMWVjLTg2NWItYmJhYTc3ZmE1ZTU0"
ct = datetime.datetime.now()
ct = ct.replace(microsecond=0)
ts = ct.strftime("%x %X")

pipe_img = Image(url='https://ci.linagora.com/uploads/-/system/project/avatar/3393/gitlab-ci-cd-logo_2x.png',
    size=ImageSize("medium"), separator=True)
greeting = TextBlock("Meraki Pipeline Complete", color="good", size="large")
fact1 = Fact(title="Completed At:", value=ts)
myfacts = FactSet(facts=[fact1])

card = AdaptiveCard(body=[pipe_img, greeting, myfacts])

wxapi = WebexTeamsAPI()
wxapi.messages.create(text="fallback", roomId=room_id, attachments=[make_attachment(card)])