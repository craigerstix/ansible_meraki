from webexteamssdk import WebexTeamsAPI
from webexteamssdk.models.cards.card import AdaptiveCard
from webexteamssdk.models.cards.inputs import Toggle, Text
from webexteamssdk.models.cards.components import TextBlock, Image
from webexteamssdk.models.cards.actions import Submit
from webexteamssdk.utils import make_attachment
from webexteamssdk.models.cards.options import ImageSize

room_id = "Y2lzY29zcGFyazovL3VzL1JPT00vNGM5MjY5NzAtZGM1NC0xMWVjLTg2NWItYmJhYTc3ZmE1ZTU0"

pipe_img = Image(url='https://img.icons8.com/color/512/ansible.png',
    size=ImageSize("small"), separator=True)
greeting = TextBlock("Merge Complete, Run Pipeline?")
wireless_toggle = Toggle(title="Configure Wireless",id="wireless_update", valueOn="true", valueOff="false")
switchport_toggle = Toggle(title="Configure Switchports",id="switchport_update", valueOn="true", valueOff="false")
network_toggle = Toggle(title="Create Networks",id="create_network", valueOn="true", valueOff="false")
target_text = Text(id="target_var", placeholder="Enter Target Inventory Host/Group")

submit = Submit(title="Start Pipeline", data={"callback_keyword": "meraki_pipe"})

card = AdaptiveCard(body=[pipe_img, greeting, wireless_toggle, switchport_toggle, network_toggle, target_text], actions=[submit])

wxapi = WebexTeamsAPI()
wxapi.messages.create(text="fallback", roomId=room_id, attachments=[make_attachment(card)])